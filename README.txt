QTools SVG

This module provides means to create and optimise SVG sprites on Acquia
environment where you cant just use grunt or gulp as youd normally would.

It contains class to aggregate SVG into sprite, and a static binary to run
svgo, where nodejs is no available.

This module is inspired by great work of the following ppls
Ken Kubiak - https://gist.github.com/kenkubiak/3627688
Adam Twardoch - https://github.com/twardoch/svgop

but keep in mind that this module only losely based on code above and 
is not interchangable with original work.
