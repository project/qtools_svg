<?php

namespace Drupal\qtools_svg\Utility;

use DOMDocument;
use DOMElement;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\qtools_common\Utility\Slug;

/**
 * Class SvgSprite.
 *
 * This class is loosely based on https://gist.github.com/kenkubiak/3627688
 * don't forget to credit original author.
 *
 * @package Drupal\sg_bcolor\Utility
 */
class SvgSprite {

  use LoggerChannelTrait;

  /**
   * The SVG Sprite document we are creating.
   *
   * @var \DOMDocument
   */
  protected $sprite;

  const SVGOP_BIN = 'bin/qtools_svgop_v200';
  const SVG_NAMESPACE = 'http://www.w3.org/2000/svg';
  const LOGGER_CHANNEL = 'qtools_svg';

  /**
   * Creates an empty SvgSprite.
   */
  public function __construct() {
    $this->sprite = new DOMDocument();
    $this->sprite->formatOutput = TRUE;
    $root = $this->sprite->createElementNS(static::SVG_NAMESPACE, 'svg');
    $this->sprite->appendChild($root);
    $root->setAttribute('xmlns', static::SVG_NAMESPACE);
  }

  /**
   * Prefixes all ids in the document.
   */
  protected function prefixId(DOMElement $element, $prefix) {
    $prefix = $prefix . '-';
    if ($element->hasAttribute('id')) {
      $element->setAttribute('id', $prefix . $element->getAttribute('id'));
    }
    foreach (['clip-path', 'mask'] as $attr) {
      if ($element->hasAttribute($attr)) {
        $ref = $element->getAttribute($attr);
        $ref = str_replace('url(#', 'url(#' . $prefix, $ref);
        $element->setAttribute($attr, $ref);
      }
    }
  }

  /**
   * Adds svg file to sprite.
   *
   * @return bool
   *   Status of the operation.
   */
  public function addFileToSprite($uri, $filename, &$errors_messages = []) {
    $prefix = Slug::slugify($filename);
    $svg = new DOMDocument();

    libxml_use_internal_errors(TRUE);
    $status = $svg->load($uri);
    $load_errors = libxml_get_errors();
    libxml_use_internal_errors(FALSE);
    if (!$status || !empty($load_errors)) {
      /** @var \LibXMLError $error */
      foreach ($load_errors as $error) {
        $errors_messages[] = $error->message;
      }
      $this->getLogger(static::LOGGER_CHANNEL)
        ->error('SVG load failed for file [@uri] with errors [@errors]', [
          '@uri' => $uri,
          '@errors' => implode(', ', $errors_messages),
        ]);
      return FALSE;
    }
    $svg_elem = $svg->documentElement;

    // Prefix file ids with slugyfied filename.
    $tag_names_with_id = ['svg', 'path', 'clipPath', 'g', 'image', 'mask'];
    foreach ($tag_names_with_id as $tag_name) {
      /** @var \DOMElement $element */
      foreach ($svg->getElementsByTagName($tag_name) as $element) {
        $this->prefixId($element, $prefix);
      }
    }

    // Update id usage (as they were changed in previous step).
    /** @var \DOMElement $element */
    foreach ($svg->getElementsByTagName('use') as $element) {
      $link_attributes = ['xlink:href', 'href'];
      foreach ($link_attributes as $attr) {
        if ($element->hasAttribute($attr)) {
          $href = $element->getAttribute($attr);
          $href = str_replace('#', '#' . $prefix . '-', $href);
          $element->setAttribute($attr, $href);
        }
      }
    }

    // Copy source <svg> element to output <symbol> element.
    $symbol = $this->sprite->createElementNS(static::SVG_NAMESPACE, 'symbol');
    $this->sprite->documentElement->appendChild($symbol);

    // Preserve viewbox and uses slugifyed name as uniqid.
    $symbol->setAttribute('viewBox', $svg_elem->getAttribute('viewBox'));
    $symbol->setAttribute('id', $prefix);

    foreach ($svg_elem->childNodes as $child) {
      if ($child->nodeType === XML_ELEMENT_NODE
        && $child->tagName !== 'metadata'
      ) {
        $symbol->appendChild($this->sprite->importNode($child, TRUE));
      }
    }

    return TRUE;
  }

  /**
   * Export sprite as string.
   *
   * @return string
   *   Sprite XML.
   */
  public function export() {
    $this->sprite->normalizeDocument();
    return $this->sprite->saveXML();
  }

}
